"""
File: eightball.py
--------------------------
This program gets a yes/no question from the user. It then generates a random number and returns the
answer associated with that number. It keeps asking the user for questions until they hit return without
entering anything.
"""

import random

# the number of different answers that can be provided by the number generator
NUM_ANSWERS = 20


def main():
    # Gets an initial question from the user.
    question = input(" I am your helpful 'yes or no question' answering Magic Eight Ball. "
                     "What would you like to know? ")

    # We will enter the loop until the user hits enter without typing a question, then the program will terminate.
    while question != "":
        # Generates a random number in the chosen range to determine which answer will be given to the user
        selection = random.randint(1, NUM_ANSWERS)

        # These are all the possible answers that can be given to the user
        if selection == 1:
            print("As I see it, yes.")
        if selection == 2:
            print("Ask again later.")
        if selection == 3:
            print("Better not tell you now.")
        if selection == 4:
            print("Cannot predict now.")
        if selection == 5:
            print("Concentrate and ask again.")
        if selection == 6:
            print("Don’t count on it.")
        if selection == 7:
            print("It is certain.")
        if selection == 8:
            print("It is decidedly so.")
        if selection == 9:
            print("Most likely.")
        if selection == 10:
            print("My reply is no.")
        if selection == 11:
            print("My sources say no.")
        if selection == 12:
            print("Outlook not so good.")
        if selection == 13:
            print("Outlook good.")
        if selection == 14:
            print("Reply hazy, try again.")
        if selection == 15:
            print("Signs point to yes.")
        if selection == 16:
            print("Very doubtful.")
        if selection == 17:
            print("Without a doubt.")
        if selection == 18:
            print("Yes.")
        if selection == 19:
            print("Yes definitely.")
        if selection == 20:
            print("You may rely on it.")

        # Prompts the user for another question until they are ready to quit
        question = input("If you now know everything just hit enter. Otherwise, what would you like to know? ")


if __name__ == '__main__':
    main()
