"""
by Dustin L from Code in Place

https://us.edstem.org/courses/490/discussion/58738

The Mandelbrot set.
This program creates a visualization of the famous Mandelbrot set.  The general
formula is z = z ** 2 + c.  For each point on the complex plane, c, an updated value
of z is calculated starting with a value of z = 0.  The formula is then repeated with
the new value of z.  This process continues until one of two conditions are met:
1. The modulus of z is greater than a predefined escape radius
2. the number of iterations reaches a (user defined) maximum iterations and the modulus
is less than the escape radius.
If the modulus never reaches the escape radius, the point is considered part of the set
and is colored black.  If the modulus reaches the escape radius before the maximum
iterations, the number of iterations is used to add color to highlight points that are
close to the set.
"""

from simpleimage import SimpleImage
import math


def main():
    width = 1000  # defines the image width
    height = 1000  # defines the image height
    mandelbrot_set = mandelbrot(width, height)
    mandelbrot_set.show()


"""
This function calculates whether a given point in the complex plane is in the Mandelbrot set.
If r or max iteration is set too large, it can take a long time to calculate every pixel.
"""


def mandelbrot(width, height):
    image = SimpleImage.blank(width, height)
    x_mid = image.width / 2
    y_mid = image.width / 2
    r = 5  # Escape radius.
    scale = 0.005  # Scales the value of each pixel. Smaller values zoom in, but takes longer.
    for pixel in image:
        x = float((pixel.x - x_mid) * scale)  # The x and y variables move the origin to the center of the image
        y = float(-(pixel.y - y_mid) * scale)
        z = complex(0, 0)  # The initial value for z
        c = complex(x, y)  # Sets the x and y pixel values to be complex numbers of form x + iy
        iteration = 0  # Initializes iteration value
        max_iteration = 1000  # How many times the equation will run before it gives up.
        while abs(z) < r ** 2 and iteration < max_iteration:
            z = z ** 2 + c
            iteration += 1
        if iteration == max_iteration:
            image.set_rgb(pixel.x, pixel.y, 0, 0, 0)
        else:
            red = int(iteration - (math.log(math.log(abs(z))) / math.log(r)))
            green = int(((max_iteration - iteration * 3) / max_iteration) * 245)
            image.set_rgb(pixel.x, pixel.y, red, green, 255)
    return image


if __name__ == '__main__':
    main()
