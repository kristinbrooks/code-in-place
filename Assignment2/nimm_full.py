"""
File: nimm_full.py
-------------------------
The Ancient Game of Nimm starts two players with a shared pile of 20 stones. They alternate turns, each taking either
one or two stones, until the pile runs out. The current_player who takes the last stone loses.

This version will let the user choose if they want to play against another person, against the computer, who
goes first, and how many stones they want to start with. They can also chose their own names if they want.
"""


def main():
    print("Welcome to the Game of Nimm!!!\n")
    print("\t\tGame Play Options:")
    print("\t\t\t1.Play against the Computer")
    print("\t\t\t2.Play against another Player\n")

    game_option = input("Enter the number of your selection: ")
    game_option = validate_1_or_2_input(game_option)

    if game_option == 1:
        player_name = input("Enter your name or hit return to leave as 'Human': ")
        if player_name == "":
            player_name = "Human"
        first_turn = input("Would you like to go first? (y/n) ")
        first_turn = validate_yes_or_no_input(first_turn)
        if first_turn == 0:
            first_player = player_name
            second_player = "Sentinel AI"
        else:
            first_player = "Sentinel AI"
            second_player = player_name

    else:
        first_player = input("Enter the first current_player's name or hit return leave it as 'Professor X': ")
        if first_player == "":
            first_player = "Professor X"
        second_player = input("Enter the second current_player's name or hit return to leave it as 'Magneto': ")
        if second_player == "":
            second_player = "Magneto"

    starting_stones = input("How many stones would you like to start with in the pile? ")
    starting_stones = validate_integer_input(starting_stones)

    winner = play_nimm_game(starting_stones, first_player, second_player)
    print_winner_message(winner)


def play_nimm_game(starting_stones, first_player, second_player):
    current_player = first_player
    number_of_stones = starting_stones
    # Each time through this loop is one turn. The game ends when there are 0 stones left so this loop will repeat
    # as long as the number of stones is positive.
    while number_of_stones > 0:
        print_current_number_of_stones(starting_stones, number_of_stones)
        # a human player's turn
        if current_player != "Sentinel AI":
            stones_taken = human_player_turn(number_of_stones, current_player)
            current_player = switch_players(current_player, first_player, second_player)
        # the ai's turn
        else:
            stones_taken = ai_turn(number_of_stones)
            current_player = switch_players(current_player, first_player, second_player)

        number_of_stones -= stones_taken  # number of stones left in the pile at the end of the turn
    return current_player


def print_current_number_of_stones(starting_stones, number_of_stones):
    if number_of_stones == 1:
        print("\nThere is 1 stone left.")
    elif number_of_stones < starting_stones:
        print(f"\nThere are {number_of_stones} stones left.")
    else:
        print(f"\nThere are {starting_stones} stones.")


def ai_turn(number_of_stones):
    if number_of_stones % 3 == 0:
        stones_taken = 2
        print("\nThe Sentinel AI took 2 stones.")
    else:
        stones_taken = 1
        print("\nThe Sentinel AI took 1 stone.")
    return stones_taken


def human_player_turn(number_of_stones, current_player):
    stones_taken = input(f"{current_player}, would you like to remove 1 or 2 stones? ")
    # check for valid input
    stones_taken = validate_1_or_2_input(stones_taken)
    if stones_taken > number_of_stones:
        print("There is only one stone left, so you can't take 2. You are taking the final stone.")
    if stones_taken == number_of_stones:
        print("You are taking the final stone.")
    return stones_taken


def switch_players(current_player, first_player, second_player):
    if current_player == first_player:
        current_player = second_player
    else:
        current_player = first_player
    return current_player


def validate_1_or_2_input(user_input):
    while (user_input != "1" and user_input != "2") or user_input == "":
        user_input = input("Please enter 1 or 2: ")
    user_input = int(user_input)
    return user_input


def validate_yes_or_no_input(answer):
    while answer not in ["YES", "Yes", "yes", "Y", "y", "NO", "No", "no", "N", "n"]:
        answer = input("Please answer yes or no: ")
    if answer in ["YES", "Yes", "yes", "Y", "y"]:
        return 0


def validate_integer_input(user_input):
    while not user_input.isdigit() or user_input == "0" or user_input == "1":
        user_input = input("Please enter an integer greater than 1: ")
    return int(user_input)


def print_winner_message(winner):
    if winner == "Sentinel AI":
        print("\nOh, no!!! The Sentinel AI won! Human logic can no longer compete...the world is lost!")
    elif winner == "Human":
        print("\nThe Human won!!! The world is saved. Human logic can still win out over artificial intelligence.")
    else:
        print(f"\n{winner} won!!! They have mastered the Game of Nimm.")


# This provided line is required at the end of a Python file
# to call the main() function.
if __name__ == '__main__':
    main()
