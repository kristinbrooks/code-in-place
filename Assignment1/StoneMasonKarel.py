from karel.stanfordkarel import *

"""
File: StoneMasonKarel.py
------------------------
StoneMasonKarel "repairs the quad" by replacing missing stones to the columns
in the supplied worlds as described in problem 2 from Assignment 1.
"""


def main():
    repair_main_quad()


# Karel moves east through the quad travelling up each column and placing beepers in each empty space
def repair_main_quad():
    while front_is_clear():
        repair_one_column()
        return_to_column_base()
        move_to_next_column()
    repair_one_column()
    return_to_column_base()


# Karel moves up one column placing beepers in any empty spots
def repair_one_column():
    turn_left()
    while front_is_clear():
        if no_beepers_present():
            put_beeper()
            move()
        else:
            move()
    if no_beepers_present():
        put_beeper()


# Karel turns around, moves back to the base of the column, and turns left in
# preparation to move to the next column
def return_to_column_base():
    turn_around()
    while front_is_clear():
        move()
    turn_left()


# Moves Karel to the base of the next column. The columns are defined in the assignment to be
# evenly spaced in all worlds, so Karel moves a set number of spaces and does not need to determine
# whether it has reached a column.
def move_to_next_column():
    for i in range(4):
        move()


# Karel turns around 180 degrees
def turn_around():
    turn_left()
    turn_left()


# There is no need to edit code beyond this point

if __name__ == "__main__":
    run_karel_program()
