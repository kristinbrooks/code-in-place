"""
This program generates the Warhol effect based on the original image.
"""

from simpleimage import SimpleImage
import random

N_ROWS = 2
N_COLS = 3
PATCH_SIZE = 222
WIDTH = N_COLS * PATCH_SIZE
HEIGHT = N_ROWS * PATCH_SIZE
MAX_COLOR_SCALE = 5
PATCH_NAME = 'images/simba-sq.jpg'


def main():
    final_image = SimpleImage.blank(WIDTH, HEIGHT)
    for row in range(N_ROWS):
        for column in range(N_COLS):
            final_image = place_patch(final_image)
    final_image.show()


def make_recolored_patch(red_scale, green_scale, blue_scale):
    """
    Implement this function to make a patch for the Warhol Filter. It
    loads the patch image and recolors it.
    red_scale: A number to multiply each pixels' red component by
    green_scale: A number to multiply each pixels' green component by
    blue_scale: A number to multiply each pixels' blue component by
    return: the newly generated patch
    """
    patch = SimpleImage(PATCH_NAME)
    for pixel in patch:
        pixel.red *= red_scale
        pixel.green *= green_scale
        pixel.blue *= blue_scale
    return patch


def place_patch(image):
    for y_coordinate in range(0, HEIGHT, PATCH_SIZE):
        for x_coordinate in range(0, WIDTH, PATCH_SIZE):
            red_scale = generate_random_color_scale(MAX_COLOR_SCALE)
            green_scale = generate_random_color_scale(MAX_COLOR_SCALE)
            blue_scale = generate_random_color_scale(MAX_COLOR_SCALE)
            current_patch = make_recolored_patch(red_scale, green_scale, blue_scale)
            for pixel in current_patch:
                image.set_pixel(pixel.x + x_coordinate, pixel.y + y_coordinate, pixel)
    return image


def generate_random_color_scale(max_scale):
    return random.uniform(0, max_scale)


if __name__ == '__main__':
    main()
