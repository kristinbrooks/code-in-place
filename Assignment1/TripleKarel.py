from karel.stanfordkarel import *

"""
File: TripleKarel.py
--------------------
TripleKarel paints the exteriors of three buildings in any of the Triple
sample worlds, as described in the Assignment 1 handout.
"""


def main():
    paint_triple_world()


# paints the sides of the three buildings in a Triple world
def paint_triple_world():
    for i in range(2):
        paint_building()
        turn_right()
    paint_building()


# paints an individual building travelling counter-clockwise
# (only paints three sides of the building as per the assignment description)
def paint_building():
    for i in range(2):
        paint_wall()
        turn_corner()
    paint_wall()


# paints a single wall of a building
def paint_wall():
    while left_is_blocked():
        put_beeper()
        move()


# turns the corner and moves Karel into position to start painting the next wall
def turn_corner():
    turn_left()
    move()


# Karel turns right
def turn_right():
    for i in range(3):
        turn_left()


# There is no need to edit code beyond this point

if __name__ == "__main__":
    run_karel_program()
