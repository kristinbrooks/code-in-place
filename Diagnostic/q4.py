def main():
    print('Enter a sequence of non-decreasing numbers.')
    num = float(input('Enter num: ')        # missing paren
    next_num = num
    length_of_sequence = 1

    while num == next_num:
        next_num = float(input('Enter num: ')   # missing paren
        if is_non_decreasing(num1, num2):   # didn't change the param names to argument names
            length_of_sequence += 1
        num = next_num

        print('Thanks fo playing!')     # shouldn't be in loop
        print('Sequence length: ' + str(length_of_sequence))    # shouldn't be in loop

    def is_non_decreasing(num1, num2):      # wrong indentation
        if num2 >= num1:
            return True
        return False


if __name__ == "__main__":
    main()
