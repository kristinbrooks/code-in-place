from simpleimage import SimpleImage


def main():
    """
    This program loads an image and applies the narok filter
    to it by setting "bright" pixels to grayscale values.
    """
    image = SimpleImage('images/simba-sq.jpg')

    # Apply the filter
    for pixel in image:
        pixel_average = (pixel.red + pixel.green + pixel.blue) // 3
        if pixel_average >= 153:
            pixel.red = pixel_average
            pixel.green = pixel_average
            pixel.blue = pixel_average

    image.show()


if __name__ == '__main__':
    main()
