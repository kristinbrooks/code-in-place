from PIL import Image
from simpleimage import SimpleImage

# Test failed for desired outcome. It makes the transparent area of the dress white, which doesn't
# allow you to see thru to the paper doll behind it. Would need a mask for each image or to use a greenscreen effect.


def main():
    twiggy = Image.open('twiggy-model.jpg')
    dress = Image.open('transparent-test.jpg')

    twiggy.paste(dress)
    twiggy.save('new_image.jpg')

    new_twiggy = SimpleImage('new_image.jpg')
    new_twiggy.show()


if __name__ == '__main__':
    main()
