from karel.stanfordkarel import *

"""
File: MidpointKarel.py
----------------------
MidpointKarel leaves a beeper on the corner closest to the center of 1st Street
(or either of the two central corners if 1st Street has an even
number of corners).  
"""
# TODO: Make Karel pick up beepers are it goes back and forth to avoid the final pickup run at the end


def main():
    find_midpoint()


# Karel find the midpoint, and stops on it with a beeper present only on the midpoint.
def find_midpoint():
    place_initial_beeper_for_one_or_two_width_worlds()
    place_beepers_in_width_three_or_greater_worlds()


# Handling worlds of width 1 or 2:
#   A beeper is placed immediately. This will be the center of a 1 width world. Then Karel attempts to move. If it
#   can't then the world is width 1. If it can only move one space, then the world is width 2. In this case Karel
#   turns around and returns to it's starting position so it is placed on a center point with and won't enter the
#   the following loop with the no_beepers_present condition. If the world is wider than two, then Karel will stay
#   where it is on the empty spot so it will enter the following loop.
def place_initial_beeper_for_one_or_two_width_worlds():
    put_beeper()
    if front_is_clear():
        move()
        if front_is_blocked():
            turn_around()
            move()


# Handling of worlds width three or greater:
#   Karel places beepers at each end of the street. Then turns and travels until it finds a beeper and places one
#   next to it. This narrows the section of open corners every time Karel makes a pass until only the midpoint has no
#   beeper. Then Karel picks up the unnecessary beepers and places one at the midpoint, then stops on the midpoint.
#   (There are some conditional if checks to not throw errors in the 1 and 2 width worlds.)
def place_beepers_in_width_three_or_greater_worlds():
    while no_beepers_present():
        if front_is_clear():
            move()
            if beepers_present():
                turn_around()
                move()
                put_beeper()
                move()
        else:
            put_beeper()
            turn_around()
            move()
    turn_around()
    if front_is_clear():
        move()
    if beepers_present():
        pick_beeper()
    place_center_beeper()
    while no_beepers_present():
        move()


# Karel moves to the wall, turns around, and proceeds across the row picking up any beepers and leaving a beeper
# on the empty center point.
def place_center_beeper():
    move_to_wall()
    retrieve_and_place_beepers()


# Karel moves along the row picking up existing beepers and placing one in any empty spots
def retrieve_and_place_beepers():
    while front_is_clear():
        if beepers_present():
            pick_beeper()
            move()
        else:
            put_beeper()
            move()
    if beepers_present():
        pick_beeper()
    else:
        put_beeper()
    turn_around()


# Karel turns around 180 degrees
def turn_around():
    turn_left()
    turn_left()


# Karel will move forward until it reaches a wall then turn around
def move_to_wall():
    while front_is_clear():
        move()
    turn_around()


# There is no need to edit code beyond this point

if __name__ == "__main__":
    run_karel_program()
