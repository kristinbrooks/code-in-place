from karel.stanfordkarel import *

'''
File: HospitalKarel.py
-----------------------
HospitalKarel builds a hospital (two columns of three beepers) in each of the locations where it finds a beeper. 
The beeper marks the lower left corner of the hospital.
'''


def main():
    build_hospitals_along_street()


def build_hospitals_along_street():
    while front_is_clear():
        if beepers_present():
            build_hospital()
        if front_is_clear():
            move()


def build_hospital():
    turn_left()
    # build first wall
    for i in range(2):
        move()
        put_beeper()
    # move over to top of second wall
    turn_right()
    move()
    turn_right()
    # build second wall
    for i in range(2):
        put_beeper()
        move()
    put_beeper()    # adds third beeper on second wall (solves fencepost problem)
    turn_left()


def turn_right():
    for i in range(3):
        turn_left()


# There is no need to edit code beyond this point

if __name__ == "__main__":
    run_karel_program()
