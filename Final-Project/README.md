Paper doll was an idea I was toying with, but eight bit avatar is the one I went with. It now 
has two separate repos on Gitlab. One for the version with the [command line interface]
(https://gitlab.com/kristinbrooks/eight-bit-avatar-cli) and one for the final version with 
the [graphical interface](https://gitlab.com/kristinbrooks/eight-bit-avatar-gui) (though it must still be started on the command line).
