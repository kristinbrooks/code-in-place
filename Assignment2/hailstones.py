"""
File: hailstones.py
-------------------
This program displays the Hailstone sequence for a number provided by the user.
"""


def main():
    number = input("Enter a number: ")
    number = validate_integer_input(number)
    count = 0
    while number != 1:
        if number % 2 == 0:
            new_number = int(number / 2)
            print(f"{number} is even, so I take half: {new_number}")
            number = new_number
        else:
            new_number = int((3 * number) + 1)
            print(f"{number} is odd, so I make 3n + 1: {new_number}")
            number = new_number
        count += 1
    print(f"The process took {count} steps to reach 1")


def validate_integer_input(user_input):
    while not user_input.isdigit() or user_input == "0" or user_input == "1":
        if not user_input.isdigit() or user_input == "0":
            user_input = input("Please enter a positive integer: ")
        else:
            user_input = input("Using 1 makes this no fun! Enter a different positive integer: ")
    return int(user_input)


# This provided line is required at the end of a Python file
# to call the main() function.
if __name__ == '__main__':
    main()
