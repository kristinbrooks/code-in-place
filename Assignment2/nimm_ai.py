"""
File: nimm_ai.py
-------------------------
The Ancient Game of Nimm starts two players with a shared pile of 20 stones. They alternate turns, each taking either
one or two stones, until the pile runs out. The current_player who takes the last stone loses. In this version the one
current_player is an AI that always wins.

(This only works because the AI goes first in combination with the number of stones they
start with in the pile.)
"""


def main():
    number_of_stones = 20   # the game always starts with 20 stones
    turn_num = 1   # even turns will be the current_player and odd turns will be the AI

    winner = play_nimm_game(number_of_stones, turn_num)

    # The game play has ended therefore the previous current_player took the last stone. This makes the other
    # current_player the winner.
    #
    # The turn_num variable was already incremented after the last turn so it was returned and assigned to the
    # winner variable. Which current_player this is will still be determined by even/odd just as the turns were.
    if winner % 2 == 0:
        print("\nThe Player wins!")
    else:
        print("\nThe AI wins!")


def play_nimm_game(number_of_stones, turn_num):
    print("There are 20 stones left")
    # The game ends when there are 0 stones left so this loop repeats as long as the number of stones is positive.
    while number_of_stones > 0:
        if number_of_stones < 20:
            print("\nThere are " + str(number_of_stones) + " stones left")
        if turn_num % 2 == 0:  # even numbered turns are current_player
            print("\nThere are " + str(number_of_stones) + " stones left")
            stones_taken = input("Player, would you like to remove 1 or 2 stones? ")

            # check for valid input
            while (stones_taken != "1" and stones_taken != "2") or stones_taken == "":
                stones_taken = input("Please enter 1 or 2: ")
            stones_taken = int(stones_taken)
            if stones_taken > number_of_stones:
                print("There is only one stone left, so you can't take 2. You are taking the final stone.")
            if stones_taken == number_of_stones:
                print("You are taking the final stone.")
        else:  # odd numbered turns are the AI
            if number_of_stones % 3 == 0:
                stones_taken = 2
            else:
                stones_taken = 1
            print("\nThe AI took " + str(stones_taken) + " stone/s.")

        number_of_stones -= stones_taken  # number of stones left in the pile at the end of the turn
        turn_num += 1  # alternates the turns
    return turn_num


# This provided line is required at the end of a Python file
# to call the main() function.
if __name__ == '__main__':
    main()
