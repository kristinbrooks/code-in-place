"""
File: khansole_academy.py
-------------------------
This program asks the user to add two randomly generated 2-digit positive integers until
they get three right answers in a row. Then it terminates.
"""

import random

# Constants defining the upper and lower range for the randomly generated integers.
SMALLEST_2_DIGIT_INT = 10
LARGEST_2_DIGIT_INT = 99


def main():
    # The count variable will track the number of correct answers the user has entered.
    count = 0
    # This loop will run as until the user has entered 3 answers correct in a row.
    while count != 3:
        num_1 = generate_random_int()
        num_2 = generate_random_int()
        total = num_1 + num_2

        # Tell the user the addition problem and get their answer.
        print("What is " + str(num_1) + " + " + str(num_2) + "?")
        user_answer = int(input("Your answer: "))

        # Check the user's answer against the correct answer and tell them if they were correct or not.
        if total == user_answer:
            count += 1
            print("Correct! You've gotten " + str(count) + " correct in a row.")
            if count == 3:
                print("Congratulations! You mastered addition.")
        else:
            # If they miss an answer the count is reset to 0 because they need 3 in a row to end the program.
            count = 0
            print("Incorrect. The expected answer is " + str(total))


# Generates a random integer in the provided range.
def generate_random_int():
    return random.randint(SMALLEST_2_DIGIT_INT, LARGEST_2_DIGIT_INT)


# This provided line is required at the end of a Python file
# to call the main() function.
if __name__ == '__main__':
    main()
