from simpleimage import SimpleImage


def main():
    image_name = input('enter an image name: ')
    image = SimpleImage('images/' + image_name)
    image.show()
    for y in range(image.height):
        for x in range(image.width):
            pixel = image.get_pixel(x, y)
            sepia_pixel(pixel)
    image.show()


def sepia_pixel(pixel):
    r = pixel.red
    g = pixel.green
    b = pixel.blue
    pixel.red = 0.393 * r + 0.769 * g + 0.189 * b
    pixel.green = 0.349 * r + 0.686 * g + 0.168 * b
    pixel.blue = 0.272 * r + 0.534 * g + 0.131 * b


if __name__ == '__main__':
    main()
