from karel.stanfordkarel import *

"""
File: CollectNewspaperKarel.py
------------------------------
CollectNewspaperKarel walks to the door of its house, picks up the
newspaper (represented by a beeper, of course), and then returns
to its initial position in the upper left corner of the house.
"""


def main():
    collect_newspaper()


# Karel moves through the house, collects the newspaper outside the door, and returns to
# its initial position in the house
def collect_newspaper():
    move_to_doorway()
    retrieve_newspaper()
    return_to_starting_position()


# Karel moves to the doorway of the house
def move_to_doorway():
    while front_is_clear():
        move()
    turn_right()
    while left_is_blocked():
        move()


# Karel moves through the door, picks up the paper, and comes back in the house
def retrieve_newspaper():
    turn_left()
    move()
    pick_beeper()
    turn_around()
    move()


# Karel moves through the house returning to its starting position and direction
def return_to_starting_position():
    turn_right()
    while front_is_clear():
        move()
    turn_left()
    while front_is_clear():
        move()
    turn_around()


# Karel turns to the right
def turn_right():
    for i in range(3):
        turn_left()


# Karel turns 180 degrees
def turn_around():
    turn_left()
    turn_left()


# There is no need to edit code beyond this point

if __name__ == "__main__":
    run_karel_program()
