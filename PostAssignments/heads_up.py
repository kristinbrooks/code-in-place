"""
How the game is played over Zoom:
    When it is your turn, close your eyes.

    A word will be displayed in the HeadsUp program.

    The rest of the section will try and describe it without saying the word.

    You have to guess the word as quickly as possible.

Milestones:
    Milestone #1: First, load all of the words from the file words.txt into a list.
    Milestone #2: Then, show a randomly chosen word from the list
    Milestone #3: Repeat: wait for the user to hit enter, then show another word.

Helpful code:
    This code reads a file line by line and prints each line. Modify it to populate a list:'

        file = open('words.txt') # file is a variable, ‘words.txt’ is a filename
        for line in file: # for-each loop gives lines one at a time
            print(line.strip()) # strip removes whitespace at the start or end

    This code selects a random element from a list (in this case the one with name my_list):

        chosen_value = random.choice(my_list) # comes with ‘import random’

Your final project could be a game like this.
"""

import random

# Name of the file to read in!
FILE_NAME = 'cswords.txt'


def main():
    # TODO: your code here
    pass


if __name__ == '__main__':
    main()
