from karel.stanfordkarel import *

"""
File: MidpointKarel.py
----------------------
MidpointKarel leaves a beeper on the corner closest to the center of 1st Street
(or either of the two central corners if 1st Street has an even
number of corners).  
"""


def main():
    while front_is_clear():
        move()
        if front_is_clear():
            move()
        turn_left()
        if front_is_clear():
            move()
        turn_right()

    turn_around()

    # NOT WORKING YET
    while front_is_clear():
        move()
        turn_left()
        if front_is_clear():
            move()
        if front_is_clear():
            move()

        turn_right()


def turn_around():
    turn_left()
    turn_left()


def turn_right():
    for i in range(3):
        turn_left()


if __name__ == "__main__":
    run_karel_program()