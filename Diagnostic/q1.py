"""
Part A: 42 will be printed. The divide_and_round function didn't return
anything, so the n in main()was unchanged and that is what was printed.
"""

"""
Part B: 
"""


def divide_and_round(n):
    """
    Divides an integer n by 2 and rounds
    up to the nearest whole number
    """
    if n % 2 == 0:
        n = n / 2
    else:
        n = (n + 1) / 2
    return n    # returned n so it can be used in main()


def main():
    n = 42
    n = divide_and_round(n)     # assigned the return value from divide_and_round() to n
    print(n)
