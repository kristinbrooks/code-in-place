"""
File: bluescreen.py
----------------
Not part of the assignment. This was a lecture demo!
This is a fun algorithm to implement. It is not in the
assignment, but feel free to implement it as an extension.
Put the smaller foreground picture into the background.
Do not include any pixels that are sufficiently blue.
"""

from simpleimage import SimpleImage

INTENSITY_THRESHOLD = 1.6


def main():
    foreground = SimpleImage('images/tiefighter.jpg')
    foreground.show()

    background = SimpleImage('images/quad.jpg')
    background.show()

    new_image = blue_screen('images/tiefighter.jpg', 'images/quad.jpg')
    new_image.show()


def blue_screen(foreground, background):
    new_image = SimpleImage(foreground)
    replacement_image = SimpleImage(background)
    for pixel in new_image:
        average = (pixel.red + pixel.green + pixel.blue) // 3
        if pixel.blue >= average * INTENSITY_THRESHOLD:
            x = pixel.x
            y = pixel.y
            new_image.set_pixel(x, y, replacement_image.get_pixel(x, y))
    return new_image


if __name__ == '__main__':
    main()
