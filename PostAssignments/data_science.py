"""
COVID19 DATA SCIENCE:

Even though the section isn’t long enough for this problem, we are including it so that you can see how the file reading
and lists allow for data analysis.

This course exists as a way to give us all a means to improve ourselves during the time of Covid-19. A big component of
our global response are “models”, often written in Python, which predict how the disease will progress. In this problem
we will take small steps towards working with that data. Much of the Covid-19 data from different countries is public.

For example we provide you with a file “italy.txt” which lists the total number of confirmed cases in Italy, one value
per day, starting on Jan 22nd. This is real data provided by Johns Hopkins Uni.

italy.txt

    0 # There were 0 confirmed cases up until Jan 22nd
    0 # There were 0 confirmed cases up until Jan 23rd
    0 # There were 0 confirmed cases up until Jan 24th
    ... # the file has 109 lines!
    215858 # the third to last line is cases up until May 7th
    217185 # the second to last line is cases up until May 8th
    218268 # There have been 218,268 cases up until May 9th

Challenges:
    Challenge #1: Load all the values from the file into a list of integers.

    Challenge #2: Count the number of non-zero values in the file (this is days since first case)

    Challenge #3: Create a list which stores how many new cases there were each day (new cases on a given day are: total
                  cases on that day - total cases on the previous day)

    These challenges are straight forward, but area  good first step into processing data via code.

In the starter code we include similar files for all reported countries.

There are a lot of interesting questions that researchers are trying to answer. Can you tell if a country has turned the
tide on the current wave of Covid-19? How can we predict the future number of cases? Can you find external data that
helps explain the number of infections? These are unsolved problems, though you are welcome to explore the data using
python and look for clues.

You could work on an analysis of this data for your final project.
"""
# Data comes from Johns Hopkins Univeristy. Thanks to them for making this public!
# https://github.com/CSSEGISandData/COVID-19
# You can find data beyond cumulative cases there!

"""
Test your code by analysing total confirmed cases over time
Each line in the file represents one day. The first value is confirmed cases on Jan 22nd.
The number of confirmed cases is "cumulative" meaning that it is the total number
of cases up until the current day. It will never go down!
"""

ITALY_PATH = 'italy.txt'

# This directory has files for all countries if you want to explore further
DATA_DIR = 'confirmed'


def main():
    # TODO: your code here
    pass


if __name__ == '__main__':
    main()
