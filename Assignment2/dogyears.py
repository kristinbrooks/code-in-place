"""
dogyears.py

This program converts human age to dog age using the common conversion 0f 1 human year = 7 dog years.
"""
DOG_YEARS_MULTIPLE = 7


# returns the age in dog years when given an age in human years
def convert_to_dog_years(human_years):
    return human_years * DOG_YEARS_MULTIPLE


def main():
    human_age = float(input('Enter human age: '))
    if human_age >= 0:
        dog_age = convert_to_dog_years(human_age)
        print('Dog age is:', dog_age)
    else:
        print('Not a valid age.')


if __name__ == '__main__':
    main()