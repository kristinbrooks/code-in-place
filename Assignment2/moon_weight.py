"""
File: moon_weight.py
--------------------
This program asks for the weight of a thing on Earth and tells the user how much it weighs on the moon.

NOTE: There were no units given in the assignment, so they were intentionally left out in the program.
"""

MOON_GRAVITY_VARIABLE = 0.165


def main():
    earth_weight = float(input("What is the weight on earth? "))

    moon_weight = earth_weight * MOON_GRAVITY_VARIABLE

    print("The weight on the moon is " + str(moon_weight) + ".")


# This provided line is required at the end of a Python file
# to call the main() function.
if __name__ == '__main__':
    main()
