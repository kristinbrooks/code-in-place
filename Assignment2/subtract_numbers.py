"""
File: subtract_numbers.py
-------------------------
This program gets two real-values from the user and prints
the first number minus the second number.
"""


def main():
    print("This program subtracts one number from another.")

    num_1 = float(input("Enter first number: "))
    num_2 = float(input("Enter second number: "))

    total = num_1 - num_2

    print("The result is " + str(total))


# This provided line is required at the end of a Python file
# to call the main() function.
if __name__ == '__main__':
    main()
