"""
Write your answer for Odd Numbers below here:
"""


def main():
    odd_num = 1
    while odd_num < 200:
        print(odd_num)
        odd_num += 2


if __name__ == "__main__":
    main()

"""
Write your answer for Can I Ride the Rollercoaster below here:
"""


def main():
    rider_height = float(input("Enter height in meters: "))
    if rider_height < 1 or rider_height > 2:
        print("You can't ride the roller coaster.")
    else:
        print("You can ride the roller coater.")


if __name__ == "__main__":
    main()
