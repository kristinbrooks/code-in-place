"""
running.py

This program keeps a running total of numbers entered by the user until the enter 0. Then the program terminates.
"""


def main():
    total = 0
    # temp value to enter the loop, overwritten by user before used
    user_number = -1
    while user_number != 0:
        user_number = float(input('Please enter a number: '))
        total += user_number
        print('The total is: ', total)


if __name__ == '__main__':
    main()
